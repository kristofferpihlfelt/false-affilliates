<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_15f4c06216b8c665dec2576fdfc6dfcb'] = 'Ogiltiga affilliate-transaktioner';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_bb8956c67b82c7444a80c6b2433dd8b4'] = 'Är du säker på att du vill avinstallera denna modul?';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_6c0bef94ad4d848b5837f52dc6c31531'] = 'Ladda upp en CSV-fil och kolla efter ogiltiga transaktioner';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_1a7dca70b7fd9d92b79f00e8cd0dfd48'] = 'Välj relevant Affilliate';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_ff6f703132d376aaeddcb61948ca34b0'] = 'Affilliate-namn';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_3336bbfa95f6be964a6c31a402334faa'] = 'Ladda upp CSV-fil från vald affilliate';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_c3112b719ff37115418799acdfed0fcb'] = 'Ladda upp CSV-fil';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_c9cc8cce247e49bae79f15173ce97354'] = 'Spara';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_59e042a8c17b1468f3f99de140cbc6cb'] = 'Lägg till nytt affilliate-nätverk';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_70f172c609d88f8a10957251ad29515c'] = 'Ange namn för affilliate-nätverk';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_7cea08a50dea19ed1506064a5f9275ee'] = 'Ange det kolumnnummer som innehåller order-id';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_349a2f103a4eccff2c448f5a1ac865e5'] = 'Kolumnnummer';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_72164e7e65f35337dbde9e200ba46ca6'] = 'Ändra affilliate';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_623ddfaf526212d3f0357cc64feb775c'] = 'Ändra namn för affilliate-nätverk';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_ce4ef4b46434d9cec7f3f415530aade1'] = 'Ända kolumn som innehåller order-id';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_06933067aafd48425d67bcb01bba5cb6'] = 'Uppdatera';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_d732794d75516f5d95d37b087201c393'] = 'Inställningar för orderstatus';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_6d154a3a186961ba30e42752dff468f1'] = 'Ange id för avbruten orderstatus ';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_9b265b715911ffea729872d7480d9226'] = 'Avbruten orderstatus';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_41da9f4e70173a73a9abce74fc6619b4'] = 'Ange id för ytterligare ogiltig orderstatus';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_209e5d4b29b7941f90575fabf58d1a67'] = 'Ytterligare ogiltig orderstatus';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_e34f72c07c31115a658acc2f0bc2e3f0'] = 'Affilliate-nätverk';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_00956d9ad10c4cb489c8471fa8378760'] = 'Kolumn för order-id';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_59715f4aa681f7671916ff23a511f2b3'] = 'Lista med dina affilliates';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_63d5049791d9d79d86e9a108b0a999ca'] = 'Referens';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_57297718fdb439175177cf6b196172d1'] = 'Orderstatus';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_cc727e8fb67cfb7da5d4c1062ac8a202'] = 'Ordervärde (exkl. moms)';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_20db0bfeecd8fe60533206a2b5e9891a'] = 'Förnamn';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_8d3f5eff9c40ee315d452392bed5309b'] = 'Efternamn';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_559e7ca805230fc80e3644f87bb3994d'] = 'Orderdatum';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_61b4cdbd848a9622429f9537007cba02'] = 'Lista med avbrutna eller ogiltiga transaktioner som hittades i CSV-filen';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_a310a72a82344155097caff1007013f4'] = 'Status måste vara en siffra';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_5d60367333ed5af62a256b955a2a6f34'] = 'Status sparades';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_bed6a1615ac42471b98862c5fa8613f3'] = 'Status nummer två sparades inte';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_2248543cab6517250cc1bd1beb2db2e4'] = 'Ogiltigt fält : Affilliate-namn';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_121d2ec98487a6972c0a49c76592dfb3'] = 'Column måste vara en siffra';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_0104d46c97a69a3b5b208ba86b6014aa'] = 'Lyckad inmatning';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_0f9c318857ce4f77167c8144c8d51c27'] = 'Borttagning misslyckades';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_5c9d74bb31a4d39d4500d88851fd06fc'] = 'Fel vid borttagning ur databas';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_472908ccfcbfa11420c96451a32bb66f'] = 'Borttagning lyckades';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_1319fd94274052c5043aa6545826181e'] = 'Uppdateringen misslyckades';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_72040829a13e93a16f8ae032ea5c6484'] = 'Lyckad uppdatering';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_d3e49bcc001e93118af256c531fc9a46'] = 'Filen kunde inte sparas';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_f9619979f434d7724a3af96dc2cec7ae'] = 'Hämtning av affilliates misslyckades';
$_MODULE['<{falseaffilliates}prestashop>falseaffilliates_62e98402fa7665ae857532330e7ba61c'] = 'Matchning av ordrar misslyckades';
$_MODULE['<{falseaffilliates}prestashop>configure_15f4c06216b8c665dec2576fdfc6dfcb'] = 'Ogiltiga affilliate-transaktioner';
$_MODULE['<{falseaffilliates}prestashop>configure_03737297426e6e019f4a6153bd4920b7'] = 'Betalar du ditt affilliate-nätverk för makulerade eller återbetalade transaktioner?';
$_MODULE['<{falseaffilliates}prestashop>configure_2ddf22a79f72dc65b9c30203b09daab9'] = 'Tack vare denna modul, kan du enkelt hitta makulerade beställningar du annars skulle betala för';
$_MODULE['<{falseaffilliates}prestashop>configure_39511afc6c6afd5bea3f81611e3ae589'] = 'Ladda ned din transaktionsrapport från ditt affilliate-nätverk';
$_MODULE['<{falseaffilliates}prestashop>configure_76306a65f1eb03513668415bb5ea994e'] = 'Ställ in id för den orderstatus som indikerar att beställningen är makulerad, återbetalad osv';
$_MODULE['<{falseaffilliates}prestashop>configure_6f999609d06474679cd5508068874383'] = 'Lägg till ditt affilliate-nätverk med namn och fältnummer från CSV-filen som innehåller beställnings-id eller beställningsreferens';
$_MODULE['<{falseaffilliates}prestashop>configure_0eb09903ada1524f6fc88cb1cdaec42e'] = 'Ladda upp CSV-filen från det valda affilliate-nätverket';
$_MODULE['<{falseaffilliates}prestashop>configure_3f0807a6d69de7d6519bb2e61feb4c73'] = 'Kolla i listan efter beställningar ditt affilliate fakturerar dig för även om de är makulerade eller återbetalade';
$_MODULE['<{falseaffilliates}prestashop>configure_6faf048195b8417d9d8be6d61f28d272'] = 'Markera beställningarna som ogiltiga, makulerade, återbetalade i kontrollpanelen hos ditt affilliate-nätverk';
$_MODULE['<{falseaffilliates}prestashop>configure_3eed047e6b508bf72e89fc88b6a327d2'] = 'Klicka på länken nedan för en ingående och detaljerad dokumentation';
$_MODULE['<{falseaffilliates}prestashop>configure_5b6cf869265c13af8566f192b4ab3d2a'] = 'Dokumentation';
$_MODULE['<{falseaffilliates}prestashop>configure_c417223686f11518d2c25e3276ccddec'] = 'Läs denna dokumentation för att konfigurera modulen';
$_MODULE['<{falseaffilliates}prestashop>configure_78463a384a5aa4fad5fa73e2f506ecfc'] = 'Engelska';
$_MODULE['<{falseaffilliates}prestashop>configure_03ba0736407106fb76e07525412ce64a'] = 'Du måste ladda upp en CSV-fil för att kolla om du har några makulerade eller ogiltiga affilliate-beställningar';
