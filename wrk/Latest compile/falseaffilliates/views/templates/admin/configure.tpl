{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Pihlfelt & Vänner AB
*  @copyright 2006-2017 Pihlfelt & Vänner AB
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($updateAffilliate)}
	{if !isset($falseOrders)}

	<div class="panel">
		<h3><i class="icon icon-credit-card"></i> {l s='False affilliate orders' mod='falseaffilliates'}</h3>
		<p>
			<strong>{l s='Are you paying your affilliate network for canceled or refunded orders?' mod='falseaffilliates'}</strong><br />
            {l s='Thanks to this module, you can easily find those canceled orders that your affilliate network otherwise will charge you for' mod='falseaffilliates'}<br />
		</p>
		<ul>
			<li>{l s='Download your transaction reports from your affilliate network' mod='falseaffilliates'}</li>
			<li>{l s='Set up module with id for the order status that is equal to canceled, refunded etc' mod='falseaffilliates'}</li>
			<li>{l s='Add your affilliate network with name and number of the field in the CSV file report that contains the order id or order reference' mod='falseaffilliates'}</li>
			<li>{l s='Upload the CSV file report from your selected affilliate network' mod='falseaffilliates'}</li>
			<li>{l s='Check list for orders your affilliate network will charge for even though they are canceled or refunded' mod='falseaffilliates'}</li>
			<li>{l s='Mark those orders as false, canceled, refunded etc at your affilliate network admin panel' mod='falseaffilliates'}</li>
		</ul>
		<br />
		<p>{l s='For detailed documentation click the links below' mod='falseaffilliates'}</p>

		<br />

		<br />

		<h3><i class="icon icon-tags"></i> {l s='Documentation' mod='falseaffilliates'}</h3>
		<p>
            {l s='Read this documentation to configure this module' mod='falseaffilliates'} :
		</p>
		<ul>
			<li><a href="../modules/falseaffilliates/readme_en.pdf" target="_blank">{l s='English' mod='falseaffilliates'}</a></li>
		</ul>
		</p>
	</div>
		{/if}

	<div class="panel">
        {if !isset($falseOrders)}
			<p> <i class="icon icon-exclamation">  </i>  {l s='You must upload a CSV file to see if you have any affilliate orders that are cancled or refunded.' mod='falseaffilliates'}</p>

        {elseif isset($falseOrders)}
            {html_entity_decode($falseOrders|escape:'htmlall':'UTF-8')}

        {/if}

	</div>

	<div class="panel">

        {$renderFalseOrdersForm|escape:'html':'UTF-8'|htmlspecialchars_decode:3} {* HTML, cannot escape*}
	</div>

	<div class="panel">
        {$listAffilliates|escape:'html':'UTF-8'|htmlspecialchars_decode:3}
	</div>

	<div class="panel">
        {$renderAffilliateForm|escape:'html':'UTF-8'|htmlspecialchars_decode:3}
	</div>

	<div class="panel">
        {$renderConfig|escape:'html':'UTF-8'|htmlspecialchars_decode:3}
	</div>

{elseif isset($updateAffilliate)}
	<div class="panel">
    	{$updateAffilliate|escape:'html':'UTF-8'|htmlspecialchars_decode:3}
	</div>

{/if}



