<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Pihlfelt & Vänner AB
*  @copyright 2006-2017 Pihlfelt & Vänner AB
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
/**
 * Defines for converting CSV files to UTF-8.
 */
define('UTF32_BIG_ENDIAN_BOM', chr(0x00) . chr(0x00) . chr(0xFE) . chr(0xFF));
define('UTF32_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE) . chr(0x00) . chr(0x00));
define('UTF16_BIG_ENDIAN_BOM', chr(0xFE) . chr(0xFF));
define('UTF16_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE));
define('UTF8_BOM', chr(0xEF) . chr(0xBB) . chr(0xBF));

class Falseaffilliates extends Module
{
    protected $config_form = false;

    private $html = '';

    public function __construct()
    {
        $this->name = 'falseaffilliates';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Pihlfelt & Vänner';
        $this->module_key = 'e44706c0621a929c64b4f1c70adc802a';
        $this->need_instance = 0;
        $this->table_name = $this->name;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('False affilliate orders');
        $this->description = $this->l('Checks uploaded csv files containing affilliate 
            networks lead/transaction reports for deleted or invalid orders. 
            That way you can mark them at your affilliate network as not completed or
            invalid and therefor save money.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }


    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */

    public function getContent()
    {
        /**
         * If values have been submitted in the forms, process.
         */
        if (((bool)Tools::isSubmit('submitNewAffilliate')) == true) {
            $this->postProcessAffilliates();
        }

        if (((bool)Tools::isSubmit('submitFalseaffilliatesConfig')) == true) {
            $this->postProcess();
        }

        if (((bool)Tools::isSubmit('deletefalseaffilliates')) == true) {
            $this->postProcessDeleteAffilliate();
        }

        if (((bool)Tools::isSubmit('updatefalseaffilliates')) == true) {
            $this->context->smarty->assign('updateAffilliate', $this->renderUpdateAffilliateForm($id = (int)Tools::getValue('id_falseaffilliates')));
        }

        if (((bool)Tools::isSubmit('submitFalseOrders')) == true) {
            $this->context->smarty->assign('falseOrders', $this->getFalseOrders());
        }

        if (((bool)Tools::isSubmit('submitUpdateAffilliate')) == true) {
            $this->postProcessUpdateAffilliate();
        }


        /**
         * Assign forms and lists to smarty template.
         */
        $this->context->smarty->assign('module_dir', $this->_path);

        /*Send array with current affilliates to smarty*/
        $this->context->smarty->assign('listAffilliates', $this->renderAffilliateList());

        /*Send the form to upload csv and select affilliate*/
        $this->context->smarty->assign('renderFalseOrdersForm', $this->renderFalseOrdersForm());

        /*Send the form to add new affilliates to smarty*/
        $this->context->smarty->assign('renderAffilliateForm', $this->renderNewAffilliateForm());

        /* Send config form to smarty */
        $this->context->smarty->assign('renderConfig', $this->renderConfigForm());


        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        /* Append template */
        return $this->html .= $output;
    }

    /**
     * Create the forms.
     */

    /*Render the form to select affilliate and upload csv files*/
    protected function renderFalseOrdersForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitFalseOrders';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(
                'csv_affilliate_name' => ''    //set default value to empty
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($this->getFalseOrdersForm()));
    }

    /*Render the form to add new affilliates*/
    protected function renderNewAffilliateForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitNewAffilliate';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(        //set default values to none
                'affilliate_name' => '',
                'csv_column_number' => ''
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($this->getNewAffilliateForm()));
    }

    /* Render the form to update affilliates */
    protected function renderUpdateAffilliateForm($id)
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitUpdateAffilliate';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getAffilliateById($id),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($this->getUpdateAffilliateForm()));
    }

    /* Render the configuration panel */
    protected function renderConfigForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitFalseaffilliatesConfig';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }


    /**
     * Create the form structure.
     */

    /* Select relevant affilliate network and upload csv */
    protected function getFalseOrdersForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Upload CSV file and check for false orders')
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'desc' => $this->l('Select relevant Affilliate'),
                        'name' => 'csv_affilliate_name',
                        'label' => $this->l('Affilliate name'),
                        'options' => array(
                            'query' => $this->getAllAffilliates(),
                            'id' => 'csv_column_number',
                            'name' => 'affilliate_name'
                        )
                    ),
                    array(
                        'type' => 'file',
                        'desc' => $this->l('Upload the CSV file from selected Affilliate'),
                        'name' => 'csv_file',
                        'label' => $this->l('Upload CSV file'),
                        'accept'=> '.csv'
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /* Add new affilliate form structure */
    protected function getNewAffilliateForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Add new affilliate')
                ),
                'input' => array(
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'desc' => $this->l('Enter name for affiliate network'),
                        'name' => 'affilliate_name',
                        'label' => $this->l('Affilliate name')
                    ),
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'desc' => $this->l('Enter csv column number containing order id'),
                        'name' => 'csv_column_number',
                        'label' => $this->l('CSV column number')
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                ),
            ),
        );
    }

    /* Add update affilliate form structure */
    protected function getUpdateAffilliateForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Edit affilliate')
                ),
                'input' => array(
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'desc' => $this->l('Update name for affiliate network'),
                        'name' => 'affilliate_name',
                        'label' => $this->l('Affilliate name')
                    ),
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'desc' => $this->l('Update csv column number containing order id'),
                        'name' => 'csv_column_number',
                        'label' => $this->l('CSV column number')
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_falseaffilliates'
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Update')
                ),
            ),
        );
    }

    /* Config form structure */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Order status settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'desc' => $this->l('Enter canceled order status id'),
                        'name' => 'FALSEAFFILLIATE_INVALID_ORDER_STATUS_1',
                        'required' => true,
                        'label' => $this->l('Canceled order status')
                    ),
                    array(
                        'col' => 2,
                        'type' => 'text',
                        'desc' => $this->l('Enter other invalid order status id'),
                        'name' => 'FALSEAFFILLIATE_INVALID_ORDER_STATUS_2',
                        'label' => $this->l('Other invalid order status')
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                ),
            ),
        );
    }


    /**
     * Set values for the inputs.
     */
    /* Get new affilliate values from form, return as array */
    protected function getAffilliateFormValues()
    {
        $affilliate = array();
        (string)$affilliate['affilliate_name'] = Tools::getValue('affilliate_name');
        (int)$affilliate[ 'csv_column_number'] = Tools::getValue('csv_column_number');

        return $affilliate;
    }

    /* Get configuration form values from config db */
    protected function getConfigFormValues()
    {
        return array(
            'FALSEAFFILLIATE_INVALID_ORDER_STATUS_1' => Configuration::get('FALSEAFFILLIATE_INVALID_ORDER_STATUS_1'),
            'FALSEAFFILLIATE_INVALID_ORDER_STATUS_2' => Configuration::get('FALSEAFFILLIATE_INVALID_ORDER_STATUS_2')
        );
    }


    /**
     * Render added affilliates list and false orders list.
     */

    /* Render a list of all current affilliates */
    private function renderAffilliateList()
    {
        $content = $this->getAllAffilliates();
        $fields_list = array(
            'id_falseaffilliates' => array(
                'title' => 'ID',
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'affilliate_name' => array(
                'title' => $this->l('Affilliate network')
            ),
            'csv_column_number' => array(
                'title' => $this->l('Order id column')
            )

        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->actions = array('edit', 'delete');
        $helper->module = $this;
        $helper->listTotal = count($content);
        $helper->identifier = 'id_falseaffilliates';
        $helper->title = $this->l('List of added affilliates');
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) .'&configure='.$this->name.
            '&tab_module='.$this->tab.'&module_name='.$this->name;

        return $helper->generateList($content, $fields_list);
    }

    /* Render a list with all matching orders that are canceled or refunded */
    private function renderFalseOrdersList($orders)
    {
        $fields_list = array(
            'id_order' => array(
                'title' => 'Order ID',
                'class' => 'fixed-width-xs'
            ),
            'reference' => array(
                'title' => $this->l('Reference'),
                'class' => 'fixed-width-xs'
            ),
            'statusname' => array(
                'title' => $this->l('Order status'),
                'class' => 'fixed-width-xs'
            ),
            'total_paid_tax_excl' => array(
                'title' => $this->l('Order value (tax excl)'),
                'class' => 'fixed-width-xs'
            ),
            'firstname' => array(
                'title' => $this->l('First name')
            ),
            'lastname' => array(
                'title' => $this->l('Last name')
            ),
            'date_add' => array(
                'title' => $this->l('Order date'),
                'class' => 'fluid-width-xs'

            )

        );

        $helper = new HelperList();
        $helper->no_link = true;
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->actions = array('view');
        $helper->module = $this;
        $helper->listTotal = count($orders);
        $helper->identifier = 'id_order';
        $helper->title = $this->l('List of canceled orders found in CSV file');
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminOrders');
        $helper->currentIndex = $this->context->link->getAdminLink('AdminOrders', true).'&id_order='.$this->identifier.
            '&vieworder';


        return $helper->generateList($orders, $fields_list);
    }

    /**
     * Post process
     */

    /* Save config values to config table */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();
        foreach ($form_values as $key => $value) {
            $status = (int)Tools::getValue((string)$key);

            if ($key == 'FALSEAFFILLIATE_INVALID_ORDER_STATUS_1' && (!$status || empty($status) || !Validate::isInt($status))) {
                $this->html .= $this->displayError($this->l('Status must be a number'));
            } elseif ($key == 'FALSEAFFILLIATE_INVALID_ORDER_STATUS_2' && !empty($status) && !Validate::isInt($status)) {
                $this->html .= $this->displayError($this->l('Second status must be a number'));
            } elseif ($key == 'FALSEAFFILLIATE_INVALID_ORDER_STATUS_2' && (empty($status) || !Validate::isInt($status))) {
                Configuration::deleteByName(pSQL($key));
                $this->html .= $this->displayError($this->l('Second status was not saved'));
            } else {
                Configuration::updateValue(pSQL($key), pSQL($status));
                $this->html .= $this->displayConfirmation($this->l('Status was Successfully saved'));
            }
        }
        return true;
    }

    /* Save values from new affilliates form to db */
    protected function postProcessAffilliates()
    {
        $form_values = $this->getAffilliateFormValues();
        $name = (string)$form_values['affilliate_name'];
        $column = (int)$form_values['csv_column_number'];

        if (!$name || empty($name) || !Validate::isGenericName($name)) {
            $this->html .= $this->displayError($this->l('Invalid field format : Affilliate name'));
        } elseif (!$column || empty($column) || !Validate::isInt($column)) {
            $this->html .= $this->displayError($this->l('Column must be a number'));
        } else {
            Db::getInstance()->insert(pSQL($this->table_name), array(
                'affilliate_name' => pSQL($form_values['affilliate_name']),
                'csv_column_number' => pSQL((int)$form_values['csv_column_number'])
            ));
            $this->html .= $this->displayConfirmation($this->l('Entry Successfully added'));
        }
    }

    /* Deletes the selected affilliate */
    protected function postProcessDeleteAffilliate()
    {
        $id_falseaffilliates = (int)Tools::getValue('id_falseaffilliates');

        if (!$id_falseaffilliates || empty($id_falseaffilliates) || !Validate::isInt($id_falseaffilliates)) {
            $this->html .= $this->displayError($this->l('Error while trying to delete entry'));
        } else {
            $id = (int)$id_falseaffilliates;

            if (!Db::getInstance()->delete(pSQL($this->table_name), 'id_falseaffilliates = ' . pSQL((int)$id))) {
                $this->html .= $this->displayError($this->l('Error while deleting from database')) . ':' . mysqli_error();
            } else {
                $this->html .= $this->displayConfirmation($this->l('Afilliate Successfully removed'));
            }
        }
    }

    /* Updates the specific affilliate in db */
    protected function postProcessUpdateAffilliate()
    {
        $form_values = $this->getAffilliateFormValues();
        $name = (string)$form_values['affilliate_name'];
        $column = (int)$form_values['csv_column_number'];

        if (!$name || empty($name) || !Validate::isGenericName($name)) {
            $this->html .= $this->displayError($this->l('Invalid field format : Affilliate name'));
        } elseif (!$column || empty($column) || !Validate::isInt($column)) {
            $this->html .= $this->displayError($this->l('Column must be a number'));
        } else {
            $id = (int)Tools::getValue('id_falseaffilliates');
            if (!$id || empty($id) || !Validate::isInt($id)) {
                $this->html .= $this->displayError($this->l('Error while updating Affilliate'));
            } else {
                Db::getInstance()->update($this->table_name, array(
                    'affilliate_name' => pSQL($name),
                    'csv_column_number' => pSQL((int)$column)
                ), 'id_falseaffilliates = '.pSQL((int)$id));
                $this->html .= $this->displayConfirmation($this->l('Entry Successfully updated'));
            }
        }
    }

    /**
     * Save CSV file, convert it to UTF-8,
     * maps it to an array with one column,
     * compares the order from CSV to orders in db,
     * finds all orders with statuses from settings,
     * returns a helper list with orders that are canceled
     */
    protected function getFalseOrders()
    {
        $path = dirname(__FILE__).'/uploads/';

        if (isset($_FILES["csv_file"]) && !empty($_FILES['csv_file'])) {
            $CSVFile = $path . basename($_FILES['csv_file']['name']);

            $fileExtension = pathinfo($CSVFile);
            if (!empty($fileExtension['extension']) && $fileExtension['extension'] !== 'csv') {
                return  $this->html .= $this->displayError($this->l('You tried to upload wrong file format, only CSV 
                    files allowed'));
            }

            // If file was uploaded
            if (move_uploaded_file($_FILES['csv_file']['tmp_name'], $CSVFile)) {
                //get contents of file
                $file = Tools::file_get_contents($CSVFile);

                //check encoding of file
                $encode = $this->fromString($file);

                //convert to utf-8
                $iso = mb_convert_encoding($file, "UTF-8", $encode);

                //save to file as utf-8
                file_put_contents($CSVFile, utf8_decode($iso));

                // map to array
                $csv = array_map('str_getcsv', file($CSVFile));

                // get values from relevant column in CSV from specific affilliate network
                $column = (int)Tools::getValue('csv_affilliate_name');

                // we only need the column containing the order id or reference
                $orderColumn = array_column($csv, $column);

                // remove the first line
                array_splice($orderColumn, 0, 1);
                // an empty array we put db values in
                $allFalseOrders = array();

                foreach ($orderColumn as $order) {
                    // if any match from db

                    if ($this->getOrderFromDb($id = $order) !== false) {
                        // get key and value pair from the db result array
                        foreach ($this->getOrderFromDb($id) as $key => $value) {
                            if (!empty($value)) {
                                $allFalseOrders[] = $value;     // put them in new array
                            }
                        }
                    }
                }

                // if uploaded and converted, display this message
                $this->html .= $this->displayConfirmation('File uploaded and converted to UTF-8');

                if (!empty($allFalseOrders)) {
                    $this->html .= $this->displayConfirmation($this->l('Your CSV file contained canceled or refunded 
                        orders. See list below.'));

                    // return a rendered list with the matching orders, those that are canceled or refunded etc
                    return $this->renderFalseOrdersList($orders = $allFalseOrders);
                } else {
                    return $this->displayConfirmation($this->l('Your CSV file doesn\'t contain any canceled or refunded 
                        orders. Congratulations, this is a good thing.'));
                }
            } else {
                return $this->html .= $this->displayError($this->l('File could not be saved'));
            }
        }
    }

    /**
     * Get data from DB.
     */
    // Get current affilliates from db
    protected function getAllAffilliates()
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.pSQL($this->table_name);

        $result = Db::getInstance()->ExecuteS(pSQL($sql));

        return $result;
    }

    /*Get all data from specific affilliate by its id*/
    protected function getAffilliateById($id)
    {
        if (!Validate::isInt($id)) {
            $this->html .= $this->displayConfirmation($this->l('Error when fetching affilliate'));
        } else {
            $result = Db::getInstance()->getRow('
                SELECT *
                FROM '._DB_PREFIX_.pSQL($this->table_name).'
                WHERE id_falseaffilliates = '.pSQL((int)$id));

            return $result;
        }
    }

    // Get orders from db with values matching from CSV file and settings
    private function getOrderFromDb($id)
    {
        $status1 = (int)Configuration::get('FALSEAFFILLIATE_INVALID_ORDER_STATUS_1');
        $status2 = (int)Configuration::get('FALSEAFFILLIATE_INVALID_ORDER_STATUS_2');

        if (!Validate::isInt($status1) && !Validate::isInt($status2) && !Validate::isGenericName($id)) {
            $this->html .= $this->displayError($this->l('Order status is not valid'));
        } else {
            if (empty($id) || $id == '') {
                return false;
            } else {
                $query = 'SELECT '._DB_PREFIX_.'orders.id_customer, '._DB_PREFIX_.'orders.id_order, 
                             '._DB_PREFIX_.'orders.reference, '._DB_PREFIX_.'orders.date_add, 
                             '._DB_PREFIX_.'orders.current_state, '._DB_PREFIX_.'orders.total_paid_tax_excl,
                             '._DB_PREFIX_.'customer.firstname, '._DB_PREFIX_.'customer.lastname, 
                             '._DB_PREFIX_.'order_state_lang.name AS statusname
                    FROM '._DB_PREFIX_.'orders
                    LEFT JOIN '._DB_PREFIX_.'customer 
                    ON '._DB_PREFIX_.'customer.id_customer = '._DB_PREFIX_.'orders.id_customer
                    LEFT JOIN '._DB_PREFIX_.'order_state_lang
                    ON '._DB_PREFIX_.'order_state_lang.id_order_state = '._DB_PREFIX_.'orders.current_state
                    WHERE '._DB_PREFIX_.'orders.current_state IN ('.pSQL($status1).','.pSQL($status2).')
                    AND '._DB_PREFIX_.'order_state_lang.id_lang = '.pSQL($this->context->language->id).'
                    AND ( ('._DB_PREFIX_.'orders.id_order =\''.pSQL($id).'\') 
                    OR ('._DB_PREFIX_.'orders.reference = \''.pSQL($id).'\') )
                    ORDER BY '._DB_PREFIX_.'orders.id_order DESC';


                $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);


                if (!empty($result)) {
                    return $result;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * A simple way to detect UTF-8/16/32 of string by its BOM (not work with string or file without BOM)
     * @return string|false encoding found
     */
    protected static function detectUtfEncoding($text)
    {
        $first2 = Tools::substr($text, 0, 2);
        $first3 = Tools::substr($text, 0, 3);
        $first4 = Tools::substr($text, 0, 3);
        if ($first3 == UTF8_BOM) {
            return 'UTF-8';
        } elseif ($first4 == UTF32_BIG_ENDIAN_BOM) {
            return 'UTF-32BE';
        } elseif ($first4 == UTF32_LITTLE_ENDIAN_BOM) {
            return 'UTF-32LE';
        } elseif ($first2 == UTF16_BIG_ENDIAN_BOM) {
            return 'UTF-16BE';
        } elseif ($first2 == UTF16_LITTLE_ENDIAN_BOM) {
            return 'UTF-16LE';
        }
        return false;
    }

    /**
     * Try to detect non UTF encodings
     * @return array all provable encodings
     */
    private static function detectNonUftEncoding($text)
    {
        $guesses = array();
        foreach (mb_list_encodings() as $item) {
            //avoid checking for UTF, 'pass' & 'auto' encodings
            if ($item === 'pass' || $item === 'auto' || strripos($item, 'UTF')!==false) {
                continue;
            }
            $sample = @iconv($item, $item, $text);
            if (Tools::strlen($sample) & md5($sample)==md5($text)) {
                $guesses[] = $item;
            }
        }
        return $guesses;
    }

    /**
     * Try to detect all provable encodings from a given string
     * @return array with encoding guesses
     */
    public static function fromString($text, $default_encoding = 'UTF-8')
    {
        $default_guess = array($default_encoding);
        if (empty($text)) {
            return $default_guess;
        }
        $guesses = self::detectNonUftEncoding($text);
        $uft = self::detectUtfEncoding($text);
        if ($uft) {
            array_unshift($guesses, $uft);
        }
        //put the default encoding at the top
        if (in_array($default_encoding, $guesses)) {
            $guesses = array_diff($guesses, $default_guess);
            array_unshift($guesses, $default_encoding);
        }
        return $guesses;
    }

    /**
     * Try to detect all provable encodings from a given file
     * @return array with encoding guesses
     */
    public static function fromFile($filename, $default_encoding = 'UTF-8')
    {
        return self::detect(@Tools::file_get_contents($filename), $default_encoding);
    }
}
